#!/bin/bash
chown -R ubuntu:www-data ./*
chmod -R 770 ./*

php bin/magento maintenance:enable

composer install --no-dev
#no need to run composer update since it should already been run in the localhost
#composer update --no-dev

bash scripts/updateModule.sh

#existing di folder may crash the app. Not sure why yet.
rm -rf var/di
rm -rf var/generation
rm -rf var/cache
rm -rf var/page_cache

php bin/magento setup:upgrade

php bin/magento setup:di:compile

# Trigger index rebuilds to make sure everything is ready before
# web server starts (reduces warnings in admin UI about indexes not
# being up to date)
php bin/magento cron:run
php bin/magento cron:run

php bin/magento indexer:reindex

# Deploy static view assets to make the start up phase faster.
php bin/magento setup:static-content:deploy en_US
#php bin/magento setup:static-content:deploy zh_Hant_HK
# php bin/magento setup:static-content:deploy zh_Hans_CN

php bin/magento cache:clean
php bin/magento cache:flush

bash scripts/helper/prodCacheEnable.sh

chown -R ubuntu:www-data ./*
chmod -R 770 ./*
php bin/magento maintenance:disable