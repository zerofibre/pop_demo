#!/bin/bash

php bin/magento cache:enable config \
collections reflection db_ddl eav customer_notification \
config_integration config_integration_api translate config_webservice \
layout block_html full_page
