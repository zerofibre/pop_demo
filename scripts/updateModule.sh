#!/usr/bin/env bash

DIRS=`ls -d ./submodules/*/`

for DIR in $DIRS
do
    bash $DIR'Update.sh'
done