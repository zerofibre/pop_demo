#!/usr/bin/env bash
bash scripts/chmod.sh
git config core.fileMode false
git config --global core.filemode false


php bin/magento maintenance:enable

#Remove --no-dev if you run for the first time, otherwise bin/magento will not be installed
composer install --no-dev
#no need to run composer update since it should already been run in the localhost
#composer update --no-dev

bash scripts/updateModule.sh

#existing cache folder may crash the app if there are compile error in previous deploy. Need to remove the cache in order to clean it.
rm -rf var/di
rm -rf var/cache
rm -rf var/generation
rm -rf var/page_cache

php bin/magento setup:upgrade

php bin/magento setup:di:compile

# Trigger index rebuilds to make sure everything is ready before
# web server starts (reduces warnings in admin UI about indexes not
# being up to date)
php bin/magento cron:run
php bin/magento cron:run

php bin/magento indexer:reindex

# Deploy static view assets to make the start up phase faster.
php bin/magento setup:static-content:deploy en_US
php bin/magento setup:static-content:deploy zh_Hant_HK
 php bin/magento setup:static-content:deploy zh_Hans_CN

php bin/magento cache:clean
php bin/magento cache:flush

bash scripts/chmod.sh
php bin/magento maintenance:disable

