<?php
namespace PrismCube\RewardPoint\Block;
use Magento\Framework\View\Element\Template;
use Magento\Customer\Model\Session;
use PrismCube\RewardPoint\Model\PointRecordFactory;


class PointRecords extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \PrismCube\RewardPoint\Model\PointRecordFactory
     */
    protected $pointRecordFactory;
    protected $_session;

    public function getCacheLifetime()
    {
        return null;
    }


    public function __construct(
        Template\Context $context,
        array $data = [],
        PointRecordFactory $pointRecordFactory,
        Session $_session
    )
    {
        parent::__construct($context, $data);
        $this->pointRecordFactory = $pointRecordFactory;
        $this->_session = $_session;
        $this->_isScopePrivate = true;
    }
    public function isCustomerLoggedIn(){
        if ($this->_session->isLoggedIn()) {
            // Customer is logged in
            return $this->_session->getCustomer()->getId();
        } else {
            // Customer is not logged in
            return false;
        }
    }

    public function getPointRecordCollection(){
        $testing = true;

        if($this->isCustomerLoggedIn() || $testing) {
            $pointRecordFactory = $this->pointRecordFactory->create()
                ->getCollection()
                //->addFieldToFilter('customer_id', ['eq' => $this->isCustomerLoggedIn()]);
                ->addFieldToFilter('customer_id', ['eq' => 1]);
            //->addFieldToSelect('entity_id','customer_id')
            return $pointRecordFactory->getData();
        }else{
            return null;
        }
    }

    public function printHello(){
        return 'Hello World';
    }

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
}