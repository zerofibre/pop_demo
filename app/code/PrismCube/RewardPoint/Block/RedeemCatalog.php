<?php
namespace PrismCube\RewardPoint\Block;
use Magento\Framework\View\Element\Template;
use Magento\Customer\Model\Session;
use PrismCube\RewardPoint\Model\PointRecordFactory;


class RedeemCatalog extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \PrismCube\RewardPoint\Model\PointRecordFactory
     */
    protected $pointRecordFactory;
    protected $_session;

    public function getCacheLifetime()
    {
        return null;
    }


    public function __construct(
        Template\Context $context,
        array $data = [],
        PointRecordFactory $pointRecordFactory,
        Session $_session
    )
    {
        parent::__construct($context, $data);
        $this->pointRecordFactory = $pointRecordFactory;
        $this->_session = $_session;
    }

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
}