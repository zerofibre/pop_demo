<?php

namespace PrismCube\RewardPoint\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

use PrismCube\RewardPoint\Model\PointRecordFactory;

class Index extends Action
{
    /**
     * @var \PrismCube\RewardPoint\Model\PointRecordFactory
     */
    protected $pointRecordFactory;

    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PointRecordFactory $modelStoreLocationFactory
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PointRecordFactory $pointRecordFactory,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->pointRecordFactory = $pointRecordFactory;
    }

    public function execute()
    {
        /**
         * When Magento get your model, it will generate a Factory class
         * for your model at var/generaton folder and we can get your
         * model by this way
         */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__('Point Record'));

        return $resultPage;
    }
}