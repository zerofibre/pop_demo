<?php

namespace PrismCube\RewardPoint\Controller\Redeem;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use \Magento\Framework\App\Request\Http;
use PrismCube\RewardPoint\Model\PointRecordFactory;
use Magento\SalesRule\Model\RuleFactory;

class Index extends Action
{
    /**
     * @var \PrismCube\RewardPoint\Model\PointRecordFactory
     */
    protected $pointRecordFactory;
    protected $rulesFactory;

    protected $request;

    /**
     * @param Context $context
     * @param PointRecordFactory $modelStoreLocationFactory
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PointRecordFactory $pointRecordFactory,
        PageFactory $resultPageFactory,
        Http $request,
        RuleFactory $ruleFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->pointRecordFactory = $pointRecordFactory;
        $this->request = $request;
        $this->ruleFactory = $ruleFactory;
    }
    public function getPost() {
        return $this->request->getPost();
    }
    public function createCoupon(){
        $ruleData = [
            "name" => "Custom Discount 2",
            "description" => "Get Custom Discount 2",
            "from_date" => null,
            "to_date" => null,
            "uses_per_customer" => "1",
            "is_active" => "1",
            "stop_rules_processing" => "1",
            "is_advanced" => "1",
            "product_ids" => null,
            "sort_order" => "0",
            "simple_action" => "by_fixed",
            "discount_amount" => "50",
            "discount_qty" => null,
            "discount_step" => "3",
            "apply_to_shipping" => "0",
            "times_used" => "0",
            "is_rss" => "0",
            "coupon_type" => "2",//2 stand for specific coupon
            "use_auto_generation" => "0",
            "uses_per_coupon" => "0",
            "simple_free_shipping" => "0",
            "customer_group_ids" => [0, 1, 2, 3],
            "website_ids" => [1],
            "coupon_code" => 'FLAT_50',
            "store_labels" => [],
            "conditions_serialized" => '',
            "actions_serialized" => ''
        ];

        $ruleModel = $this->ruleFactory->create();
        $ruleModel->setData($ruleData);
        $ruleModel->save();
    }
    public function execute()
    {
        $postData = $this->getPost();
        if(isset($postData['catalog-coupon-id'])){
            echo 'Got Coupon ID';
            $this->createCoupon();
        }else{
            echo 'Got No Coupon ID';
        }

    }
}