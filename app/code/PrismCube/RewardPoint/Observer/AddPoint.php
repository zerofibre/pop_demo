<?php
namespace PrismCube\RewardPoint\Observer;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use PrismCube\RewardPoint\Model\PointRecordFactory;//Use Factory for Database Record Create


class AddPoint implements ObserverInterface
{
    protected $pointRecordFactory;

    public function __construct(
        PointRecordFactory $pointRecordFactory
    )
    {
        $this->pointRecordFactory = $pointRecordFactory;
    }

    public function execute(Observer $observer)
    {
        ob_start();
        $file = '/var/www/html/popular/addpoint_observer.log';
        $fd = fopen($file,'a');
        echo <<<XOT
==================================
Add Point::execute: Start!!
==================================

XOT;

        //Get Event Object
        $eventObject = $observer->getEvent();

        //Get Invoice Object
        $invoice = $eventObject->getInvoice();

        //Get Order
        $order = $invoice->getOrder();

        echo 'Order Status: '.$order->getState()."\r\n";

        //Get Total Paid
        $total_paid = $order->getTotalPaid();

        echo 'Total Paid:'.$total_paid."\r\n";


        //Get Customer ID
        //Check Is guest checkout or not
        if($order->getCustomerIsGuest()){
            //Guest User, no reward point is added
        }else{
            /* Sample Create Business Entity to Database */
            //1. Create a object
            $newPointRecord = $this->pointRecordFactory->create();

            //2. Set Data
            $newPointRecord->setData('customer_id',$order->getCustomerId());
            $newPointRecord->setData('point_value',$total_paid);
            $newPointRecord->setData('description',$order->getIncrementId());
            $newPointRecord->setData('created_at',date("Y-m-d H:i:s",time()));
            $newPointRecord->setData('status',1); //1 stand-for Active

            //3. Save Record
            $newPointRecord->save();
        }

        echo <<<XOT
        
==================================
Add Point::execute: End!!
==================================

XOT;
        $response = ob_get_contents();
        fwrite($fd,$response);
        ob_end_flush();
        fclose($fd);
        return $this;
    }

}