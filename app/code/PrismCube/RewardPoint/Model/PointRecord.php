<?php
namespace PrismCube\RewardPoint\Model;
use Magento\Framework\Model\AbstractModel;

class PointRecord extends AbstractModel
{
    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('PrismCube\RewardPoint\Model\ResourceModel\PointRecord');
    }
}
