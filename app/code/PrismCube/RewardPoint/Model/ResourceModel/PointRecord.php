<?php
namespace PrismCube\RewardPoint\Model\ResourceModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * StoreLocation mysql resource.
 */
class PointRecord extends AbstractDb
{
    /**
     * Define main table
     */
    protected function _construct()
    {
        $this->_init('prismcube_rewardpoint_record', 'entity_id');
    }
}
