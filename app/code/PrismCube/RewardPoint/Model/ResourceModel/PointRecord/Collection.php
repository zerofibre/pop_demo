<?php
namespace PrismCube\RewardPoint\Model\ResourceModel\PointRecord;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'PrismCube\RewardPoint\Model\PointRecord',
            'PrismCube\RewardPoint\Model\ResourceModel\PointRecord'
        );
    }
}
