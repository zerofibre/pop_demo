<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_CustomRegistration
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\CustomRegistration\Model\Customer\Attribute\Backend;

class File extends \Webkul\CustomRegistration\Model\Customer\Attribute\Backend\Image
{
    /**
     * @var string
     */
    protected $_type = 'file';
}
